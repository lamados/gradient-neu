# Gradient

A colourful Discord bot, made with [DiscordGo](https://github.com/bwmarrin/discordgo).

# Roadmap

- [X] colour role giving
- [ ] colour previewing
- [ ] colour modification:
    - lighten/darken
    - saturate/desaturate
    - colour tinting
    - invert
- [ ] automatic cleanup
- [ ] automatic re-adding of roles on rejoining
