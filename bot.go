package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/muesli/gamut/palette"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Bot struct {
	Session *discordgo.Session
	Log     *Logger
	DB      *gorm.DB
	Queue   *Queue
	Config  Config

	Commands map[string]Command
}

func NewBot(configPath string) (*Bot, error) {
	raw, err := os.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("read file: %w", err)
	}

	var config Config

	err = json.Unmarshal(raw, &config)
	if err != nil {
		return nil, fmt.Errorf("unmarshal json: %w", err)
	}

	bot := new(Bot)

	return bot, bot.Init(config)
}

func (bot *Bot) Init(config Config) (err error) {
	// set up logger.
	bot.Log = NewLogger("BOT", config.LogLevel)

	// set up client.
	bot.Session, err = discordgo.New("Bot " + config.Discord.BotToken)
	if err != nil {
		return fmt.Errorf("failed to open session: %w", err)
	}

	// connect to database.
	bot.DB, err = gorm.Open(sqlite.Open(config.Database.DSN))
	if err != nil {
		return fmt.Errorf("connect to database: %w", err)
	}

	// set database logger.
	bot.DB.Logger = bot.Log.NewGORMLogger()

	// automatically set up tables.
	err = bot.DB.Transaction(func(tx *gorm.DB) error {
		return tx.AutoMigrate(&GuildSettings{}, &Cooldown{})
	})
	if err != nil {
		return fmt.Errorf("database setup: %w", err)
	}

	// set up queue.
	bot.Queue = NewQueue(config.LogLevel)

	// set up commands.
	bot.Commands = map[string]Command{
		"about":   &About{},
		"cleanup": &Cleanup{},
		"give":    &Give{Palette: palette.AllPalettes()},
		"help":    &Help{},
		"ping":    &Ping{},
		"prefix":  &Prefix{Default: config.Discord.DefaultPrefix},
		"setup":   &Setup{},
	}

	bot.Config = config

	// return.
	return nil
}

func (bot *Bot) Run() (err error) {
	bot.Log.Alert("bot is starting up.")
	defer bot.Log.Alert("bot has shut down.")

	// set up automated database cleanup.
	bot.Log.Info("setting up auto-cleanup loop...")

	cleanupTicker := time.NewTicker(time.Hour)
	defer cleanupTicker.Stop()

	go func(ticker *time.Ticker) {
		for {
			select {
			case <-ticker.C:
				err := bot.CleanupCooldowns()
				if err != nil {
					bot.Log.Error("failed to clean up cooldowns: " + err.Error())
				}
			}
		}
	}(cleanupTicker)

	// open session.
	bot.Log.Info("opening session...")

	err = bot.Session.Open()
	if err != nil {
		bot.Log.Error("failed to open session: " + err.Error())
	}
	defer func() {
		if err == nil {
			bot.Log.Info("closing session...")

			err = bot.Session.Close()
			if err != nil {
				bot.Log.Error("failed to close session: " + err.Error())
			}
		}
	}()

	// set up job queue.
	bot.Log.Info("starting job queue...")
	go bot.Queue.Run()

	// add handlers.
	bot.Log.Info("adding event handlers...")

	bot.Session.AddHandler(bot.handlerGuildCreate)
	bot.Session.AddHandler(bot.handlerMessageCreate)

	// wait for ctrl+c then shut down.
	bot.Log.Alert("bot has started up.")
	bot.Log.Info("press ctrl+c to exit.")

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c

	// wait for jobs to finish.
	bot.Log.Info("waiting for jobs to finish...")
	bot.Queue.WG.Wait()

	// return.
	return
}
