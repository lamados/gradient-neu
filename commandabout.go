package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

type About struct{}

func (*About) Name() string {
	return "about"
}

func (*About) Description() string {
	return "Show about information for the bot, server, user, etc."
}

func (*About) Tags() []CommandTag {
	return []CommandTag{
		CommandTagGeneral,
	}
}

func (*About) Cooldown() time.Duration {
	return time.Second * 3
}

func (about *About) Run(bot *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error) {
	subject := "bot"

	if len(commandArgs) > 0 {
		subject = strings.ToLower(strings.Join(commandArgs, " "))
	}

	response := new(discordgo.MessageEmbed)

	// set response content.
	switch subject {
	case "here", "server":
		// get guild (try from state cache first).
		guild, err := bot.Session.State.Guild(message.GuildID)
		if err != nil {
			// get guild from API.
			guild, err = bot.Session.Guild(message.GuildID)
			if err != nil {
				return nil, err
			}
		}

		// add guild name.
		subject = guild.Name

		// add thumbnail.
		response.Thumbnail = &discordgo.MessageEmbedThumbnail{URL: guild.IconURL()}

		// add member count.
		if guild.MemberCount > 0 {
			response.Fields = append(response.Fields, &discordgo.MessageEmbedField{
				Name:  strconv.Itoa(guild.MemberCount) + " members",
				Value: "<@" + guild.OwnerID + "> is the owner.",
			})
		}

		// add role count.
		if len(guild.Roles) > 0 {
			response.Fields = append(response.Fields, &discordgo.MessageEmbedField{
				Name:  strconv.Itoa(len(guild.Roles)) + " roles",
				Value: "None of them are interesting.",
			})
		}

		// add channel count.
		if len(guild.Channels) > 0 {
			response.Fields = append(response.Fields, &discordgo.MessageEmbedField{
				Name:  strconv.Itoa(len(guild.Channels)) + " channels",
				Value: "None of them are interesting.",
			})
		}

		// add emoji count and list of all emojis.
		pt := 1
		emojiFieldName := strconv.Itoa(len(guild.Emojis)) + " emojis"
		emojiField := &discordgo.MessageEmbedField{Name: emojiFieldName + " (pt. " + strconv.Itoa(pt) + ")"}

		for _, emoji := range guild.Emojis {
			mention := emoji.MessageFormat()

			if len(emojiField.Value)+len(mention) >= 1024 {
				response.Fields = append(response.Fields, emojiField)

				pt++
				emojiField = &discordgo.MessageEmbedField{Name: emojiFieldName + " (pt. " + strconv.Itoa(pt) + ")"}
			} else {
				emojiField.Value += mention
			}
		}

		response.Fields = append(response.Fields, emojiField)
	case "you", "bot":
		subject = "me"

		response.Description = "There's nothing interesting about me.\nAlternatively the about command is still not fully implemented yet."
	case "user", "me":
		subject = "you"

		response.Description = "There's nothing interesting about you.\nAlternatively the about command is still not fully implemented yet."
	default:
		return nil, fmt.Errorf("unknown subject \"" + subject + "\"")
	}

	// set title.
	response.Title = "About " + subject

	return response, nil
}
