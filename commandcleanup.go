package main

import (
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/lamados/empty"
	"golang.org/x/exp/maps"
)

type Cleanup struct{}

func (c *Cleanup) Name() string {
	return "cleanup"
}

func (c *Cleanup) Description() string {
	return "Removed unused colour roles."
}

func (c *Cleanup) Tags() []CommandTag {
	return []CommandTag{
		CommandTagAdmin,
		CommandTagRoles,
	}
}

func (c *Cleanup) Cooldown() time.Duration {
	return 10 * time.Minute
}

func (c *Cleanup) Run(bot *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error) {
	// get all managed roles, role indices, etc.
	_, colourRoles, _, _, err := bot.getRoles(message.GuildID)
	if err != nil {
		return nil, err
	}

	colourRoleIDsMap := make(map[string]empty.Type)
	for _, colourRole := range colourRoles {
		colourRoleIDsMap[colourRole.ID] = empty.Value
	}

	// get all guild members.
	members, err := getAllMembers(bot.Session, message.GuildID)
	if err != nil {
		return nil, err
	}

	// remove role ID from the map if any member has it.
	for _, member := range members {
		for _, colourRoleID := range member.Roles {
			delete(colourRoleIDsMap, colourRoleID)
		}
	}

	// all IDs of roles that should be removed.
	colourRoleIDs := maps.Keys(colourRoleIDsMap)

	// add job to cleanup roles.
	bot.Queue.AddJobs(Job{
		ID: "cleanupRoles",
		Func: func() error {
			var removed int

			for _, colourRoleID := range colourRoleIDs {
				err := bot.Session.GuildRoleDelete(message.GuildID, colourRoleID)
				if err != nil {
					bot.Log.Warn("failed to delete role: " + err.Error())
				} else {
					removed++
				}
			}

			_, _ = bot.Session.ChannelMessageSendEmbed(message.ChannelID, &discordgo.MessageEmbed{
				Title:       "Success",
				Description: "Removed " + strconv.Itoa(removed) + " unused roles.",
			})

			return nil
		},
	})

	// return response.
	return &discordgo.MessageEmbed{
		Title:       "Started",
		Description: "Cleaning up unused roles.\nThis may take some time.",
	}, nil
}

func getAllMembers(session *discordgo.Session, guildID string) ([]*discordgo.Member, error) {
	const limit = 1000

	var members []*discordgo.Member
	var after string

	for {
		ms, err := session.GuildMembers(guildID, after, limit)
		if err != nil {
			return nil, err
		}

		if len(ms) == 0 {
			break
		}

		members = append(members, ms...)
		after = ms[len(ms)-1].User.ID
	}

	return members, nil
}
