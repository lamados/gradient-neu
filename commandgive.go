package main

import (
	"errors"
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/martinlindhe/base36"
	"github.com/muesli/gamut"
	"gitlab.com/lamados/empty"
	"gitlab.com/lamados/random"
	"golang.org/x/exp/maps"
)

var (
	ErrInvalidHexChar    = errors.New("invalid hexadecimal character")
	ErrInvalidHexFormat  = errors.New("invalid hexadecimal format")
	ErrInvalidColourName = errors.New("invalid colour name")
)

type Give struct {
	Palette gamut.Palette
}

func (*Give) Name() string {
	return "give"
}

func (*Give) Description() string {
	return "Give the user a new colour."
}

func (*Give) Tags() []CommandTag {
	return []CommandTag{
		CommandTagRoles,
	}
}

func (*Give) Cooldown() time.Duration {
	return time.Second * 5
}

func (give *Give) PreRun(_ *Bot, commandArgs []string, _ *discordgo.Message) error {
	return validateArgCount(commandArgs, 1, -1)
}

func (give *Give) Run(bot *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error) {
	colourInt, err := give.parseColour(commandArgs[0])
	if err != nil {
		return nil, err
	}

	var targetIDs []string
	if len(commandArgs) > 1 {
		for _, arg := range commandArgs[1:] {
			targetIDs = append(targetIDs, strings.Trim(arg, "<!@>"))
		}
	} else {
		targetIDs = []string{message.Author.ID}
	}

	// get all managed roles, role indices, etc.
	roles, colourRoles, start, _, err := bot.getRoles(message.GuildID)
	if err != nil {
		return nil, err
	}

	// find if colour role already exists, else create new colour role.
	var colourRole *discordgo.Role
	var isNew bool
	for _, role := range colourRoles {
		if role.Color == colourInt {
			colourRole = role
			break
		}
	}
	if colourRole == nil {
		colourRole, err = bot.Session.GuildRoleCreate(message.GuildID)
		if err != nil {
			return nil, err
		}
		colourRole, err = bot.Session.GuildRoleEdit(message.GuildID, colourRole.ID, random.Word(), colourInt, false, 0, false)
		if err != nil {
			return nil, err
		}
		colourRole.Position = roles[start].Position
		isNew = true
	}

	// give new colour role to user.
	for _, targetID := range targetIDs {
		err = bot.Session.GuildMemberRoleAdd(message.GuildID, targetID, colourRole.ID)
		if err != nil {
			return nil, err
		}
	}

	// queue up reordering roles.
	if isNew {
		bot.Queue.AddJobs(Job{
			ID: "reorderRoles",
			Func: func() error {
				_, err := bot.Session.GuildRoleReorder(message.GuildID, append(roles, colourRole))
				return err
			},
		})
	}

	// queue up removing existing colour roles.
	bot.Queue.AddJobs(Job{
		ID: "removeRoles",
		Func: func() error {
			member, err := bot.Session.GuildMember(message.GuildID, message.Author.ID)
			if err != nil {
				return err
			}

			roleMap := make(map[string]empty.Type)
			for _, roleID := range member.Roles {
				roleMap[roleID] = empty.Value
			}

			for _, role := range colourRoles {
				if role.ID == colourRole.ID {
					continue
				}

				delete(roleMap, role.ID)
			}

			err = bot.Session.GuildMemberEdit(member.GuildID, member.User.ID, maps.Keys(roleMap))
			return err
		},
	})

	// return response.
	response := &discordgo.MessageEmbed{Title: "Success"}
	response.Color = colourInt
	hex := fmt.Sprintf("%06X", colourInt)
	if isNew {
		response.Description = "Given new colour \"" + colourRole.Name + "\" (`#" + hex + "`)."
	} else {
		response.Description = "Given existing colour \"" + colourRole.Name + "\" (`#" + hex + "`)."
	}
	response.Description += "\nIt may take a moment for previous role(s) to be removed."
	return response, nil
}

func (give *Give) parseColour(s string) (i int, err error) {
	if s == "random" {
		return rand.Intn(16777215) + 1, nil // nolint:gosec
	}

	rgbToInt := func(r, g, b int) int {
		return (r << 16) + (g << 8) + b
	}

	switch s[0] {
	case '#':
		hexToByte := func(b byte) byte {
			switch {
			case b >= '0' && b <= '9':
				return b - '0'
			case b >= 'a' && b <= 'f':
				return b - 'a' + 10
			case b >= 'A' && b <= 'F':
				return b - 'A' + 10
			default:
				err = ErrInvalidHexChar
				return 0
			}
		}

		var r, g, b int

		switch len(s) {
		case 4: // CSS 3-digit hex color notation
			r = int(hexToByte(s[1]) * 17)
			g = int(hexToByte(s[2]) * 17)
			b = int(hexToByte(s[3]) * 17)
		case 7: // CSS 6-digit hex color notation
			r = int((hexToByte(s[1]) << 4) + hexToByte(s[2]))
			g = int((hexToByte(s[3]) << 4) + hexToByte(s[4]))
			b = int((hexToByte(s[5]) << 4) + hexToByte(s[6]))
		default: // invalid or unsupported hex format
			err = ErrInvalidHexFormat
		}

		i = rgbToInt(r, g, b)
	case '%':
		i = int(base36.Decode(s)) % 0xFFFFFF
	default:
		colours := give.Palette.Filter(s)
		if len(colours) == 0 {
			return 0, ErrInvalidColourName
		}

		r, g, b, _ := colours[0].Color.RGBA()

		i = rgbToInt(int(r), int(g), int(b))
	}

	return
}

func (bot *Bot) getRoles(guildID string) (roles, colourRoles []*discordgo.Role, start, end int, error error) {
	start, end = -1, -1

	// get guild settings.
	settings, err := bot.GetGuildSettings(guildID)
	if err != nil {
		return nil, nil, start, end, err
	}

	// get all currently managed roles.
	roles, err = bot.Session.GuildRoles(guildID)
	if err != nil {
		return nil, nil, start, end, err
	}

	// sort roles by their .Position value.
	sort.Slice(roles, func(i, j int) bool { return roles[i].Position < roles[j].Position })

	// get start and end indexes.
	for i, role := range roles {
		switch role.ID {
		case settings.ColourStartRoleID:
			bot.Log.Debug("start = " + role.Name)
			start = i
		case settings.ColourEndRoleID:
			bot.Log.Debug("end = " + role.Name)
			end = i
		}
	}
	if end >= start || start == -1 || end == -1 {
		return nil, nil, start, end, fmt.Errorf("roles not set up correctly, please re-run setup")
	}

	return roles, roles[end+1 : start], start, end, nil
}
