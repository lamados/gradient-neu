package main

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

type Help struct{}

func (*Help) Name() string {
	return "help"
}

func (*Help) Description() string {
	return "Display help information."
}

func (*Help) Tags() []CommandTag {
	return []CommandTag{
		CommandTagGeneral,
	}
}

func (*Help) Cooldown() time.Duration {
	return time.Second * 1
}

func (help *Help) Run(bot *Bot, commandArgs []string, _ *discordgo.Message) (interface{}, error) {
	if len(commandArgs) == 0 {
		// create and add commands to help embed.
		response := &discordgo.MessageEmbed{Title: "Help"}

		for _, command := range bot.Commands {
			response.Fields = append(response.Fields, &discordgo.MessageEmbedField{
				Name:   command.Name(),
				Value:  command.Description(),
				Inline: true,
			})
		}

		// sort help embed.
		sort.Slice(response.Fields, func(i, j int) bool {
			return response.Fields[i].Name < response.Fields[j].Name
		})

		return response, nil
	}

	commandName := strings.Join(commandArgs, " ")

	command, ok := bot.LookupCommand(commandName)
	if !ok {
		return nil, fmt.Errorf("no command with name \"%s\"", commandName)
	}

	return &discordgo.MessageEmbed{
		Title:       "Help: " + commandName,
		Description: command.Description(),
		Fields: []*discordgo.MessageEmbedField{
			{Name: "Tags", Value: strings.Join(command.Tags(), ", ")},
			{Name: "Cooldown", Value: command.Cooldown().String(), Inline: true},
		},
	}, nil
}
