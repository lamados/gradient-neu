package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

type Ping struct {
	messageTime  time.Time
	processTime  time.Time
	responseTime time.Time
}

func (*Ping) Name() string {
	return "ping"
}

func (*Ping) Description() string {
	return "Responds with \"Pong!\" to confirm that the bot is still running."
}

func (*Ping) Tags() []CommandTag {
	return []CommandTag{
		CommandTagGeneral,
	}
}

func (*Ping) Cooldown() time.Duration {
	return time.Second * 3
}

func (ping *Ping) Run(_ *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error) {
	// set times.
	ping.processTime = time.Now()
	ping.messageTime = message.Timestamp

	if len(commandArgs) == 0 {
		return "Pong!", nil
	}

	msg := strings.Join(commandArgs, " ")

	if len(msg) >= 280 {
		return "You talk too much.", nil
	}

	return msg + " to you too!", nil
}

func (ping *Ping) PostRun(bot *Bot, _ []string, _ *discordgo.Message, response *discordgo.Message) error {
	ping.responseTime = response.Timestamp

	_, err := bot.Session.ChannelMessageEdit(response.ChannelID, response.ID, fmt.Sprintf(
		"%s %dms / %dms",
		response.Content,
		ping.processTime.Sub(ping.messageTime).Milliseconds(),
		ping.responseTime.Sub(ping.processTime).Milliseconds(),
	))
	return err
}
