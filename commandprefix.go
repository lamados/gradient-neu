package main

import (
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

type Prefix struct {
	Default string
}

func (*Prefix) Name() string {
	return "prefix"
}

func (*Prefix) Description() string {
	return "Sets the command prefix for this server."
}

func (*Prefix) Tags() []CommandTag {
	return []CommandTag{
		CommandTagAdmin,
	}
}

func (*Prefix) Cooldown() time.Duration {
	return time.Second * 1
}

func (prefix *Prefix) PreRun(_ *Bot, commandArgs []string, _ *discordgo.Message) error {
	return validateArgCount(commandArgs, 1, 1)
}

func (prefix *Prefix) Run(bot *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error) {
	newPrefix := commandArgs[0]

	// change prefix.
	settings, err := bot.GetGuildSettings(message.GuildID)
	if err != nil {
		return nil, err
	}

	if strings.EqualFold(newPrefix, "reset") {
		settings.Prefix = prefix.Default
	} else {
		settings.Prefix = newPrefix
	}

	// update in database.
	bot.UpdateGuildSettings(settings)

	return "Prefix set to `" + settings.Prefix + "`.", nil
}
