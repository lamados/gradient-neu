package main

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gorm.io/gorm"
)

type PreCommand interface {
	Command

	// PreRun runs before checking for command cooldown status, so should only be used to validate command arguments or perform other light checks.
	PreRun(bot *Bot, commandArgs []string, message *discordgo.Message) error
}

type Command interface {
	Name() string
	Description() string
	Tags() []CommandTag
	Cooldown() time.Duration

	// Run is a command function that is always available and does a majority of the processing.
	Run(bot *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error)
}

type PostCommand interface {
	Command

	// PostRun runs after a Run command and is usually used to update message content.
	PostRun(bot *Bot, commandArgs []string, message *discordgo.Message, response *discordgo.Message) error
}

var ErrCommandNotFound = errors.New("command not found")

func (bot *Bot) LookupCommand(commandName string) (Command, bool) {
	command, ok := bot.Commands[commandName]
	if !ok {
		return nil, false
	}

	// return command.
	return command, true
}

func (bot *Bot) RunCommand(_ *discordgo.Session, commandName string, commandArgs []string, message *discordgo.Message) error {
	bot.Log.Debug(commandName + "(" + strings.Join(commandArgs, ", ") + ")")
	start := time.Now()

	// get command.
	command, ok := bot.LookupCommand(commandName)
	if !ok {
		return ErrCommandNotFound
	}

	// send typing notifier (errors can be ignored since it doesn't affect functionality).
	_ = bot.Session.ChannelTyping(message.ChannelID)

	// check if there's an active cooldown.
	cooldown, err := bot.GetCooldown(message.GuildID, message.Author.ID, commandName)
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return err
		}
	}

	if start.Before(cooldown.Until) {
		s := cooldown.Until.Sub(start).Round(time.Second).String()
		return fmt.Errorf("cooldown still active for another %s", s)
	}

	// run pre-command if available.
	if preCommand, ok := command.(PreCommand); ok {
		err := preCommand.PreRun(bot, commandArgs, message)
		if err != nil {
			return err
		}
	}

	// apply cooldown.
	err = bot.SetCooldown(message.GuildID, message.Author.ID, command)
	if err != nil {
		return err
	}

	// run command.
	responseContent, err := command.Run(bot, commandArgs, message)
	if err != nil {
		return err
	}

	var response *discordgo.Message

	switch r := responseContent.(type) {
	case *discordgo.MessageEmbed:
		// set the embed author and footer.
		avatarURL := message.Author.AvatarURL("128")

		r.Author = &discordgo.MessageEmbedAuthor{
			Name:    message.Author.String(),
			IconURL: avatarURL,
		}

		avatarURL = bot.Session.State.User.AvatarURL("128")

		r.Footer = &discordgo.MessageEmbedFooter{
			Text:    bot.Session.State.User.String(),
			IconURL: avatarURL,
		}

		// send the returned embed.
		response, err = bot.Session.ChannelMessageSendComplex(message.ChannelID, &discordgo.MessageSend{
			Embed:     r,
			Reference: message.Reference(),
		})
	case nil:
		// send generic "command completed successfully" message.
		response, err = bot.Session.ChannelMessageSendComplex(message.ChannelID, &discordgo.MessageSend{
			Embed: &discordgo.MessageEmbed{Title: "Command run successfully.",
				Color: 0x00FF00,
			},
			Reference: message.Reference(),
		})
	default:
		// send string representation of whatever was returned.
		response, err = bot.Session.ChannelMessageSendComplex(message.ChannelID, &discordgo.MessageSend{
			Content:   fmt.Sprintf("%+v", r),
			Reference: message.Reference(),
		})
	}
	if err != nil {
		return err
	}

	// run post-command if available.
	if postCommand, ok := command.(PostCommand); ok {
		err = postCommand.PostRun(bot, commandArgs, message, response)
		if err != nil {
			return err
		}
	}

	// return.
	return nil
}

func validateArgCount(args []string, min, max int) error {
	count := len(args)

	if count > max && max != -1 {
		return fmt.Errorf("too many arguments, expected a maximum of %d", max)
	}

	if count < min && min != -1 {
		return fmt.Errorf("too few arguments, expected a minimum of %d", min)
	}

	return nil
}
