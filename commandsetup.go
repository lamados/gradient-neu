package main

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
)

type Setup struct{}

func (s *Setup) PreRun(_ *Bot, commandArgs []string, _ *discordgo.Message) error {
	return validateArgCount(commandArgs, 1, -1)
}

func (s *Setup) Name() string {
	return "setup"
}

func (s *Setup) Description() string {
	return "Ensure that the server is set up correctly for commands."
}

func (s *Setup) Tags() []CommandTag {
	return []CommandTag{
		CommandTagAdmin,
	}
}

func (s *Setup) Cooldown() time.Duration {
	return time.Second * 10
}

func (s *Setup) Run(bot *Bot, commandArgs []string, message *discordgo.Message) (interface{}, error) {
	command := commandArgs[0]
	switch command {
	case "give":
		settings, err := bot.GetGuildSettings(message.GuildID)
		if err != nil {
			return nil, err
		}

		if len(commandArgs) > 2 {
			guildRoles, err := bot.Session.GuildRoles(message.GuildID)
			if err != nil {
				return nil, err
			}

			var startRole, endRole *discordgo.Role
			for _, guildRole := range guildRoles {
				switch guildRole.Name {
				case commandArgs[1]:
					startRole = guildRole
				case commandArgs[2]:
					endRole = guildRole
				}
			}

			if startRole == nil {
				return nil, fmt.Errorf("role with name \"" + commandArgs[1] + "\" could not be found, please re-run setup")
			}
			settings.ColourStartRoleID = startRole.ID

			if endRole == nil {
				return nil, fmt.Errorf("role with name \"" + commandArgs[2] + "\" could not be found, please re-run setup")
			}
			settings.ColourEndRoleID = endRole.ID

			bot.UpdateGuildSettings(settings)
		} else {
			if settings.ColourStartRoleID == "" || settings.ColourEndRoleID == "" {
				response := &discordgo.MessageEmbed{
					Title: "Setup: " + command,
					Description: "Create 2 new roles and place them next to each other in the role list.\n" +
						"Then re-run this command along with the names of the roles.\n" +
						"Roles created by the `give` command will be places between the 2 specified roles.",
					Fields: []*discordgo.MessageEmbedField{
						{
							Name:   "Example",
							Value:  "`" + settings.Prefix + "setup give ColourStart ColourEnd`",
							Inline: true,
						},
					},
				}
				return response, nil
			}

			guildRoles, err := bot.Session.GuildRoles(message.GuildID)
			if err != nil {
				return nil, err
			}

			var startRole, endRole *discordgo.Role
			for _, guildRole := range guildRoles {
				switch guildRole.ID {
				case settings.ColourStartRoleID:
					startRole = guildRole
				case settings.ColourEndRoleID:
					endRole = guildRole
				}
			}

			if startRole == nil {
				return nil, fmt.Errorf("start role could not be found, please re-run setup")
			}

			if endRole == nil {
				return nil, fmt.Errorf("end role could not be found, please re-run setup")
			}
		}
	default:
		if _, ok := bot.LookupCommand(command); !ok {
			return nil, fmt.Errorf("unknown setup command \"" + command + "\"")
		}
		return "`" + command + "` does not require any additional setup", nil
	}

	return "Command is ready to run.", nil
}
