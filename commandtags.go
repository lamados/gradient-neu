package main

type CommandTag = string

const (
	CommandTagGeneral CommandTag = "general"
	CommandTagRoles   CommandTag = "roles"
	CommandTagAdmin   CommandTag = "admin"
)
