package main

type Config struct {
	Database DatabaseConfig `json:"database" env:"DATABASE"`
	Discord  DiscordConfig  `json:"discord" env:"DISCORD"`

	LogLevel int `json:"log_level" env:"LOG_LEVEL"`
}

type DatabaseConfig struct {
	DSN string `json:"dsn" env:"DSN"`
}

type DiscordConfig struct {
	BotToken string `json:"bot_token" env:"BOT_TOKEN"`
	AppID    string `json:"app_id" env:"APP_ID"`

	DefaultPrefix string `json:"default_prefix" env:"DEFAULT_PREFIX"`
}
