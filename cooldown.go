package main

import (
	"time"

	"gorm.io/gorm"
)

type Cooldown struct {
	GuildID     string    `gorm:"column:guild_id;primaryKey"`
	UserID      string    `gorm:"column:user_id;primaryKey"`
	CommandName string    `gorm:"column:command_name;primaryKey"`
	Until       time.Time `gorm:"column:until;not null"`
}

func (bot *Bot) GetCooldown(guildID, userID string, commandName string) (cooldown *Cooldown, err error) {
	err = bot.DB.Transaction(func(tx *gorm.DB) (err error) {
		return tx.Where("guild_id = ? AND user_id = ? AND command_name = ?", guildID, userID, commandName).First(&cooldown).Error
	})

	return
}

func (bot *Bot) SetCooldown(guildID, userID string, command Command) (err error) {
	err = bot.DB.Transaction(func(tx *gorm.DB) (err error) {
		return tx.Save(&Cooldown{
			GuildID:     guildID,
			UserID:      userID,
			CommandName: command.Name(),
			Until:       time.Now().Add(command.Cooldown()),
		}).Error
	})

	return
}

func (bot *Bot) CleanupCooldowns() (err error) {
	bot.Log.Info("running cooldown cleanup...")

	err = bot.DB.Transaction(func(tx *gorm.DB) (err error) {
		tx = tx.Where("until < ?", time.Now()).Delete(&Cooldown{})
		if tx.Error != nil {
			return tx.Error
		}

		bot.Log.Info("cleaned up ", tx.RowsAffected, " rows.")

		return nil
	})

	return
}
