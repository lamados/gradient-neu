module gitlab.com/lamados/gradient/v3

go 1.18

require (
	github.com/bwmarrin/discordgo v0.25.0
	github.com/fatih/color v1.13.0
	github.com/martinlindhe/base36 v1.1.0
	github.com/muesli/gamut v0.3.0
	github.com/spf13/pflag v1.0.5
	gitlab.com/lamados/empty v0.0.0-20220522105823-e31c91a059f5
	gitlab.com/lamados/random v0.0.0-20220410221610-c1537d3124a6
	golang.org/x/exp v0.0.0-20220518171630-0b5c67f07fdf
	gorm.io/driver/sqlite v1.3.2
	gorm.io/gorm v1.23.5
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.12 // indirect
	github.com/muesli/clusters v0.0.0-20200529215643-2700303c1762 // indirect
	github.com/muesli/kmeans v0.3.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
	golang.org/x/text v0.3.7 // indirect
)
