package main

import (
	"context"
	"fmt"
	"time"

	"gorm.io/gorm/logger"
)

type GORMLogger struct {
	logger *Logger
}

func (logger *Logger) NewGORMLogger() *GORMLogger {
	return &GORMLogger{logger}
}

func (gormLogger *GORMLogger) LogMode(level logger.LogLevel) logger.Interface {
	switch level {
	case logger.Silent:
		gormLogger.logger.Level = LogDebug
	case logger.Info:
		gormLogger.logger.Level = LogInfo
	case logger.Warn:
		gormLogger.logger.Level = LogWarn
	case logger.Error:
		gormLogger.logger.Level = LogError
	default:
		// do nothing
	}

	return gormLogger
}

func (gormLogger *GORMLogger) Trace(_ context.Context, begin time.Time, fc func() (string, int64), err error) {
	if err != nil {
		elapsed := time.Since(begin)
		sql, rows := fc()

		gormLogger.logger.Debug(fmt.Sprintf("database: elapsed: %s, rows: %d, sql: %s", elapsed.String(), rows, sql))
	}
}

func (gormLogger *GORMLogger) Info(_ context.Context, s string, i ...interface{}) {
	gormLogger.logger.Info("database: " + fmt.Sprintf(s, i...))
}

func (gormLogger *GORMLogger) Warn(_ context.Context, s string, i ...interface{}) {
	gormLogger.logger.Warn("database: " + fmt.Sprintf(s, i...))
}

func (gormLogger *GORMLogger) Error(_ context.Context, s string, i ...interface{}) {
	gormLogger.logger.Error("database: " + fmt.Sprintf(s, i...))
}
