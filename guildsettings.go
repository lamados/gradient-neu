package main

import (
	"database/sql"
	"errors"
	"time"

	"gorm.io/gorm"
)

type GuildSettings struct {
	GuildID string `gorm:"column:guild_id;primaryKey"`

	CreatedAt time.Time
	UpdatedAt time.Time

	Prefix            string `gorm:"column:prefix;not null"`
	ColourStartRoleID string `gorm:"column:colour_start_role_id;not null;default:''"`
	ColourEndRoleID   string `gorm:"column:colour_end_role_id;not null;default:''"`
}

func (bot *Bot) GetGuildSettings(guildID string) (settings *GuildSettings, err error) {
	err = bot.DB.Transaction(func(tx *gorm.DB) (err error) {
		err = tx.Where("guild_id = ?", guildID).First(&settings).Error
		if errors.Is(err, sql.ErrNoRows) {
			settings.GuildID = guildID
			settings.Prefix = bot.Config.Discord.DefaultPrefix

			err = tx.Create(&settings).Error
		}

		return
	})

	if settings.Prefix == "" {
		settings.Prefix = bot.Config.Discord.DefaultPrefix
	}

	return
}

func (bot *Bot) UpdateGuildSettings(settings *GuildSettings) {
	bot.Queue.AddJobs(Job{
		ID: "updateGuildSettings",
		Func: func() error {
			return bot.DB.Transaction(func(tx *gorm.DB) (err error) {
				return tx.Save(settings).Error
			})
		},
	})
}
