package main

import (
	"errors"

	"github.com/bwmarrin/discordgo"
	"gorm.io/gorm"
)

func (bot *Bot) handlerGuildCreate(_ *discordgo.Session, event *discordgo.GuildCreate) {
	err := bot.DB.Transaction(func(tx *gorm.DB) (err error) {
		// find existing guild settings.
		err = tx.Where("guild_id = ?", event.Guild.ID).First(&GuildSettings{}).Error
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = tx.Create(&GuildSettings{
				GuildID: event.Guild.ID,
			}).Error
		}

		return
	})
	if err != nil {
		bot.Log.Error(err)
	}
}
