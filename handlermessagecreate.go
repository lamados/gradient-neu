package main

import (
	"errors"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (bot *Bot) handlerMessageCreate(_ *discordgo.Session, message *discordgo.MessageCreate) {
	settings, err := bot.GetGuildSettings(message.GuildID)
	if err != nil {
		bot.Log.Error(err)
		return
	}

	// remove prefix from message content.
	content, hasPrefix := TrimIfPrefix(message.Content, settings.Prefix)

	// ignore messages which have no prefix.
	if !hasPrefix {
		return
	}

	// split message content.
	commandArgs := strings.Fields(content)

	// ignore blank messages.
	if len(commandArgs) == 0 {
		return
	}

	// run command.
	commandName := commandArgs[0]
	commandArgs = commandArgs[1:]

	err = bot.RunCommand(bot.Session, commandName, commandArgs, message.Message)
	if err != nil {
		// silently ignore message if command not found.
		if errors.Is(err, ErrCommandNotFound) {
			return
		}

		bot.Log.Error("failed to run command: " + err.Error())

		_, err := bot.Session.ChannelMessageSendComplex(message.ChannelID, &discordgo.MessageSend{
			Embed: &discordgo.MessageEmbed{
				Title:       "Failed to run command!",
				Description: err.Error(),
				Color:       0xFF0000,
			},
			Reference: message.Reference(),
		})
		if err != nil {
			bot.Log.Error("failed to send response: " + err.Error())
		}
	}
}
