package main

import (
	"log"
	"os"

	"github.com/fatih/color"
)

const (
	LogSilent = iota - 1
	LogError
	LogWarn
	LogInfo
	LogDebug
)

var (
	prefixAttribs = color.Bold

	prefixFmt = color.New(prefixAttribs, color.FgWhite)

	debugPreFmt = color.New(prefixAttribs, color.FgHiBlack)
	infoPreFmt  = color.New(prefixAttribs, color.FgHiBlue)
	warnPreFmt  = color.New(prefixAttribs, color.FgHiYellow)
	errorPreFmt = color.New(prefixAttribs, color.FgHiRed)
	alertPreFmt = color.New(prefixAttribs, color.FgHiGreen)

	messageFmt = color.New(color.FgHiWhite)
	debugFmt   = color.New(color.FgWhite)
)

type Logger struct {
	Level int

	logger *log.Logger
}

func NewLogger(name string, level int) *Logger {
	return &Logger{
		Level:  level,
		logger: log.New(os.Stderr, prefixFmt.Sprintf("[%s] ", name), log.LstdFlags|log.Lmsgprefix),
	}
}

func (logger *Logger) Debug(v ...interface{}) {
	if logger.Level >= LogDebug {
		logger.logger.Println(debugPreFmt.Sprint("[DBUG] ") + debugFmt.Sprint(v...))
	}
}

func (logger *Logger) Info(v ...interface{}) {
	if logger.Level >= LogInfo {
		logger.logger.Println(infoPreFmt.Sprint("[INFO] ") + messageFmt.Sprint(v...))
	}
}

func (logger *Logger) Warn(v ...interface{}) {
	if logger.Level >= LogInfo {
		logger.logger.Println(warnPreFmt.Sprint("[WARN] ") + messageFmt.Sprint(v...))
	}
}

func (logger *Logger) Error(v ...interface{}) {
	if logger.Level >= LogError {
		logger.logger.Println(errorPreFmt.Sprint("[ERRR] ") + messageFmt.Sprint(v...))
	}
}

func (logger *Logger) Alert(v ...interface{}) {
	if logger.Level >= LogError {
		logger.logger.Println(alertPreFmt.Sprint("[ALRT] ") + messageFmt.Sprint(v...))
	}
}
