package main

import (
	"encoding/json"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/spf13/pflag"
)

func init() {
	// seed RNG.
	rand.Seed(time.Now().UnixNano())
}

func main() {
	genConfig := pflag.Bool("generate-config", false, "generate a blank config.json file")
	pflag.Parse()

	if *genConfig {
		raw, err := json.MarshalIndent(&Config{
			Database: DatabaseConfig{},
			Discord:  DiscordConfig{},
			LogLevel: 0,
		}, "", "  ")
		if err != nil {
			panic(err)
		}

		err = os.WriteFile("config.json", raw, 0600)
		if err != nil {
			panic(err)
		}

		os.Exit(0)
	}

	bot, err := NewBot("config.json")
	if err != nil {
		panic(err)
	}

	err = bot.Run()
	if err != nil {
		panic(err)
	}
}

func TrimIfPrefix(s, prefix string) (string, bool) {
	if strings.HasPrefix(s, prefix) {
		return s[len(prefix):], true
	}

	return s, false
}
