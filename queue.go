package main

import (
	"sync"
)

type Queue struct {
	Jobs chan Job
	Log  *Logger

	WG sync.WaitGroup
}

type Job struct {
	ID   string
	Func func() error
}

func NewQueue(logLevel int) *Queue {
	return &Queue{
		Jobs: make(chan Job),
		Log:  NewLogger("QUEUE", logLevel),
		WG:   sync.WaitGroup{},
	}
}

func (queue *Queue) AddJobs(jobs ...Job) {
	queue.WG.Add(len(jobs))

	for _, job := range jobs {
		queue.Log.Info("adding job " + job.ID + " to queue...")
		queue.Jobs <- job
	}
}

func (queue *Queue) Run() {
	queue.Log.Alert("queue is waiting for jobs.")

	for {
		select {
		case job := <-queue.Jobs:
			queue.Log.Info("job " + job.ID + " running...")

			go func() {
				err := job.Func()
				if err != nil {
					queue.Log.Error("job " + job.ID + " failed: " + err.Error())
				}

				queue.Log.Info("job " + job.ID + " finished.")
				queue.WG.Done()
			}()
		}
	}
}
